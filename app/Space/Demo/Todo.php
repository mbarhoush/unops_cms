<?php
namespace Laraspace\Space\Demo;

use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
	public function user(){
		return $this->belongsTo('Laraspace\User');
	}
}
