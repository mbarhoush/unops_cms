<?php
namespace Laraspace\Http\Controllers\Demo;

use Illuminate\Http\Request;
use Illuminate\View\View;
use Laraspace\Http\Requests;
use Laraspace\Http\Controllers\Controller;
use Laraspace\Space\Demo\Todo;
use Laraspace\User;

class TodosController extends Controller
{
    /**
     * Todos Index
     *
     * @return View
     */
    public function index()
    {
        $todos = Todo::all();
        $users = User::all();
        foreach ($todos as  $todo){
        	$todo->user =   $todo->user()->select('id', 'name')->first();
        }


        return view('admin.pages.todos.index')
	        ->with('todos', $todos)
	        ->with('users', $users)
	        ;
    }

    /**
     * Save Todo
     *
     * @param Requests\TodosRequest $request
     * @return View
     */
    public function store(Requests\TodosRequest $request)
    {
        $todo = new Todo();
	    $todo->user_id  =   $request->user_id;
        $todo->title = $request->title;
        $todo->completed = $request->completed;
        $todo->save();

        return response()->json([
            'message' => 'Todo Added Successfully',
            'todo' => $todo
        ], 200);
    }

    /**
     * Toggle the Todo
     *
     * @param $id
     * @param Request $request
     * @return View
     */
    public function toggleTodo($id, Request $request)
    {
        $todo = Todo::findOrFail($id);
        $todo->completed = $request->completed;
        $todo->save();

        return response()->json([
            'message' => 'Todo Marked as Complete',
        ], 200);
    }

    /**
     * Delete Todo
     *
     * @param $id
     * @return View
     */
    public function destroy($id)
    {
        $todo = Todo::findOrFail($id);
        $todo->delete();

        return response()->json([
            'message' => 'Todo Deleted Successfully',

        ], 200);
    }
}
