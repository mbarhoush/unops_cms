## Introduction

This is an INCOMPLETE simple CMS that contains TODO tasks module for Employer and Employees, built in PHP Larvel 5 and Vuejs. 


## How to Install
- Run `composer install` <br />
- Open `.env` file in your favorite text editor and set the database credentials. <br />
- Open `database/seeds/UsersTableSeeder.php` and change the admin's email & password to your preference. <br />
- `php artisan migrate --seed` run this command to migrate & seed the database. ( Make sure that you're inside the app's root directory ). <br />
- Install NPM globally if you haven't installed that already. <br />
- After installing NPM globally , run `npm install` inside your webroot , it will download all the required dependencies. <br />
- Run `npm run dev` for compiling sass and js files. <br />
- Run `php artisan serve` and you are ready to go! <br />

